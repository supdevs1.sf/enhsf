# -*- coding: utf-8 -*-
# Copyright (c) 2015, Shi Fang HK and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

import datetime
from frappe.utils import now_datetime, nowdate
import json

from enhsf.projects.doctype.task.task import get_task_name 

class TimesheetEnh(Document):
	pass

@frappe.whitelist()
def submit_ts(json_data):
	# http://yan.desktop.shifanghk.com.hk:8501/api/method/enhsf.enhsf.doctype.timesheetenh.timesheetenh.submit_ts?....
	
	#timesheet = frappe.get_doc({"doctype":"Timesheet", "note": "test1"})
	timesheet = frappe.new_doc("Timesheet")
	timesheet.note = ""
	
	ts_value = json.loads(json_data)
	# 2) 
	for x in xrange(len(ts_value)):
		timesheet_detail = timesheet.append('time_logs', {})
		
		# "selected_activity"_type attribute may be empty
		try:
			timesheet_detail.activity_type = ts_value[x]["selected_activity_type"]
		except KeyError:
			# do nothing for no input for those keys
			pass
		
		# reference : http://stackoverflow.com/questions/2721782/how-to-convert-a-date-string-to-a-datetime-object
		timesheet_detail.from_time = frappe.utils.data.convert_utc_to_user_timezone(
																				datetime.datetime.strptime(ts_value[x]["datetimeValueStartTime"][:-5], '%Y-%m-%dT%H:%M:%S')			# need to remove the milliseconds fr. Javascript moment.js to python datetime 
																				)
		timesheet_detail.hours = ts_value[x]["hours"]
		timesheet_detail.to_time = frappe.utils.data.convert_utc_to_user_timezone(
																				datetime.datetime.strptime(ts_value[x]["datetimeValueEndTime"][:-5], '%Y-%m-%dT%H:%M:%S') 
																				)
		
		timesheet_detail.project = ts_value[x]["selected_project"]
		timesheet_detail.task = get_task_name(ts_value[x]["selected_task"]);					# should get the link (task name)
		
		# "note" attribute may be empty
		try:
			timesheet_detail.note = ts_value[x]["note"]
		except KeyError:
			# do nothing for no input for those keys
			pass
	
	timesheet.submit()
	frappe.db.commit()

"""
This Codebase Version
example:  curl -k -b cookie_file https://localhost:9001/api/method/enhsf.enhsf.doctype.timesheetenh.timesheetenh.getVer
"""
@frappe.whitelist()
def getVer():
    return '{ version: "v1.1.0"}'