# Copyright (c) 2013, Shi Fang HK and contributors
# For license information, please see license.txt

# Command from API by following:
#	login and saving into cookie
#		wget --no-check-certificate --save-cookies dev_cookies.txt --keep-session-cookies https://localhost:9004/api/method/login --post-data 'usr=Administrator&pwd=shifang1'
#	export by excel format
#		wget --no-check-certificate --content-on-error --load-cookies dev_cookies.txt https://localhost:9004/api/method/frappe.desk.query_report.export_query --post-data 'file_format_type=Excel&filters={"from_date":"2017-10-01","to_date":"2017-10-31","type":"Maintenance"}&report_name=AmonicsTimesheet' -O a.xlsx
#
#	Filter Parameters:
#		filters > type: [Maintenance] / [Development]
#		filters > from_date / to_date: YYYY-MM-DD
#		-O a.xlsx (working excel) / a.html (see error msg)
#
#	NON-TESTED
#		curl --insecure -c cookie.txt -d "usr=Administrator" -d "pwd=shifang1" https://localhost:9004/api/method/login
#		curl --insecure -b cookie.txt -X POST -d 'file_format_type=Excel&filters={"from_date":"2017-08-01","to_date":"2017-08-03"}&report_name=Amonics_Timesheet' https://localhost:9004/api/method/frappe.desk.query_report.export_query
#
#	This Function involve to: 
#		/frappe/frappe/public/js/frappe/views/reports/query_report.js > make_export() 
#	
#		frappe.desk.query_report.export_query()
#			bench --site site1.local execute frappe.desk.query_report.export_query

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.desk.reportview import build_match_conditions

def execute(filters=None):
	if not filters:
		filters = {}
	elif filters.get("from_date") or filters.get("to_date"):
		filters["from_time"] = "00:00:00"
		filters["to_time"] = "24:00:00"

	#columns, data = [], []
	columns = get_column()
	conditions = get_conditions(filters)
	data = get_data(conditions, filters)
	
	return columns, data

def get_column():
    # [Frappe Official Script Report](https://frappe.io/docs/user/en/guides/reports-and-printing/how-to-make-script-reports.html)
    #   [Width] -- config the Report width of web display
    # the following sequency should be same to get_data()
	return [ _("Hours") + "::50", "::20", _("Task") + "::150", _("Sub-Cat") + "::300", "::20", "::20", _("Note") + "::600",
		_("From Datetime") + "::140", _("To Datetime") + "::140", _("Activity Type") + "::120",
		_("Timesheet") + ":Link/Timesheet:120", _("Employee Name") + "::150"]

# Get table data from db
#   Fixed criteria for this Project (Amonics Timesheet)
def get_data(conditions, filters):

    # This data filtering for Amonics Project Only. 
	#  adding proj_name filtering ==> as "Amonics IT support" only
	filters['proj_name'] = "%Amonics IT support%"
	
	# verify `tabTimesheet Detail`.activity_type item
	#	handling %(type)s == NULL, then should search for both development and maintenance
	if (filters.get('type') == None):
		where_type_sql = ''
	else:
		where_type_sql = 'and `tabTimesheet Detail`.activity_type = %(type)s'
	
	#print conditions
	#print filters
	
	# Query db
	sql_str = ("""select `tabTimesheet Detail`.hours, '', `tabTask`.subject, `tabTimesheet Detail`.sub_category, '', '', `tabTimesheet Detail`.note, 
		`tabTimesheet Detail`.from_time, `tabTimesheet Detail`.to_time, `tabTimesheet Detail`.activity_type, `tabTimesheet`.name, `tabTimesheet`.employee_name
		from `tabTimesheet Detail`, `tabTimesheet`, `tabTask`
		where `tabTimesheet Detail`.parent = `tabTimesheet`.name and %s and `tabTask`.name = `tabTimesheet Detail`.task 
		and `tabTimesheet Detail`.project like %s %s
		order by `tabTask`.subject ASC, `tabTimesheet Detail`.from_time ASC"""%(
		  conditions,
		  '%(proj_name)s',
		  where_type_sql,
		  )
		)
	#print sql_str
	time_sheet = frappe.db.sql(sql_str, filters, as_list=1)

	return time_sheet

def get_conditions(filters):
	conditions = "`tabTimesheet`.docstatus = 1"
	if filters.get("from_date"):
		conditions += " and `tabTimesheet Detail`.from_time >= timestamp(%(from_date)s, %(from_time)s)"
	if filters.get("to_date"):
		conditions += " and `tabTimesheet Detail`.to_time <= timestamp(%(to_date)s, %(to_time)s)"

	match_conditions = build_match_conditions("Timesheet")
	if match_conditions:
		conditions += " and %s" % match_conditions

	return conditions