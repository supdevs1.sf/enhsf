# Copyright (c) 2016, Shi Fang Hong Kong Limited Co. and Contributors
# License: GNU General Public License v3. See license.txt

import frappe

def get_task_name(task_subject_value):
	"""Returns task name by task subject searching from database. 
			return [ 
						[u'TASK00020', ... ], 
						...  
						]
			so using task_name_arr[0][0]
				1st task_name_arr[0]	==> [u'TASK00020', ... ]
				2nd [0] ==> TASK00020 
	"""
	task_name_arr = frappe.db.sql("""select name
		 from `tabTask`
		where ( `tabTask`.subject = %s )
		""", task_subject_value, as_list=True)
	return task_name_arr[0][0]